//
// Created by Tomasz Rzepka on 18/05/15.
//

#include "FileReader.h"

using namespace std;

bool FileReader::openFile(string p_name) {
    m_ifstream.open(p_name, ifstream::in);
    return m_ifstream.good();
}

FileReader::~FileReader() {
    if (m_ifstream.is_open())
        m_ifstream.close();
}

std::string FileReader::getFirst100() {
    if(m_str.empty())
        readFile();
    return m_str.substr(0,100);

}

std::string FileReader::getDocument() {
    if(m_str.empty())
        readFile();
    return m_str;
}

void FileReader::readFile() {
    string tmp;
    while(m_ifstream.good()) {
        getline(m_ifstream,tmp);
        m_str+=tmp;
    }
}
