#include <iostream>
#include <memory>
#include "IFileReader.h"
#include "Creator.h"
#include <chrono>
using namespace std;

int main() {
    Creator c;
    unique_ptr<IFileReader> reader = c.createReader();
    auto start = chrono::high_resolution_clock::now();
    cout<<reader->openFile("/home/dojo/Workspace/ClionProjects/Proxy/text")<<endl;
    reader->getFirst100();
    auto readPart = chrono::high_resolution_clock::now();
    reader->getDocument();
    auto stop = chrono::high_resolution_clock::now();

    cout<<"part: "<<chrono::duration_cast<chrono::microseconds>(readPart-start).count()<<endl
        <<"whole: "<<chrono::duration_cast<chrono::microseconds>(stop-start).count()<<endl;
    return 0;
}