//
// Created by Tomasz Rzepka on 18/05/15.
//

#pragma once

#include "FileReader.h"
#include <memory>

class SmartFileReader: public IFileReader {
public:
    SmartFileReader();
    virtual bool openFile(std::string p_name) override;
    virtual std::string getFirst100() override;
    virtual std::string getDocument() override;

private:
    std::unique_ptr<FileReader> m_basicReader;
};
