//
// Created by Tomasz Rzepka on 18/05/15.
//

#pragma once

#include <fstream>
#include "IFileReader.h"

class FileReader: public IFileReader {

public:
    virtual bool openFile(std::string p_name) override;
    virtual ~FileReader();
    void readFile();
    virtual std::string getFirst100() override;
    virtual std::string getDocument() override;
    std::string m_str;
    std::ifstream m_ifstream;
};
