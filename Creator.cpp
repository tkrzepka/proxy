//
// Created by Tomasz Rzepka on 18/05/15.
//

#include "Creator.h"

using namespace std;

std::unique_ptr<IFileReader> Creator::createReader() {
    return move(make_unique<SmartFileReader>());
    //return move(make_unique<FileReader>());
}
