//
// Created by Tomasz Rzepka on 18/05/15.
//

#pragma once

#include <memory>
#include "SmartFileReader.h"

class Creator {
public:
    std::unique_ptr<IFileReader> createReader();
};
