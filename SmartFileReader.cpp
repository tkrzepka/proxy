//
// Created by Tomasz Rzepka on 18/05/15.
//

#include "SmartFileReader.h"

using namespace std;

bool SmartFileReader::openFile(std::string p_name) {
    m_basicReader->openFile(p_name);
}

std::string SmartFileReader::getFirst100() {
    string tmp;
    while(m_basicReader->m_ifstream.good() && m_basicReader->m_str.length()<100) {
        getline(m_basicReader->m_ifstream,tmp);
        m_basicReader->m_str+=tmp;
    }
    return m_basicReader->m_str.substr(0,100);
}

std::string SmartFileReader::getDocument() {
    string tmp;
    while(m_basicReader->m_ifstream.good()) {
        getline(m_basicReader->m_ifstream,tmp);
        m_basicReader->m_str+=tmp;
    }
    return m_basicReader->m_str;
}

SmartFileReader::SmartFileReader() {
    m_basicReader = make_unique<FileReader>();
}
