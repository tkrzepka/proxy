//
// Created by Tomasz Rzepka on 18/05/15.
//

#pragma once

#include <string>

class IFileReader{
public:
    virtual ~IFileReader() { }
    virtual bool openFile(std::string p_name) = 0;
    virtual std::string getFirst100() = 0;
    virtual std::string getDocument() = 0;
};